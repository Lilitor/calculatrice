package com.example.calculatrice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class MainActivity extends AppCompatActivity {

    Button buttonInfo;
    Button buttonClear;
    Button button1;
    Button button2;
    Button button3;
    Button button4;
    Button button5;
    Button button6;
    Button button7;
    Button button8;
    Button button9;
    Button button0;
    Button buttonDot;
    Button buttonSign;
    Button buttonPercent;
    Button buttonSlash;
    Button buttonTime;
    Button buttonMinus;
    Button buttonPlus;
    Button buttonEqual;

    private String memory = "0";
    private String seizure = null;
    private String op = null;
    private double res = 0;

    private BigDecimal result = null;
    private BigDecimal term1 = null;
    private BigDecimal term2 = null;
    private double t1 = 0;
    private double t2 = 0;

    private boolean conc = false;

    private EditText ptResult = null;
    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ptResult = findViewById(R.id.printResult);

        buttonInfo = findViewById(R.id.info);

        buttonClear = findViewById(R.id.clear_b);
        buttonClear.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                memory= "0";
                seizure = null;
                op = null;
                res = 0;
                t1 = 0;
                t2 = 0;
                conc = false;
                ptResult.setText(memory);
            }
        });

        buttonSign = findViewById(R.id.sign_b);
        buttonSign.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (null == seizure) {
                    seizure = "0";
                } else {
                    if (seizure.startsWith("-")) {
                        seizure = seizure.substring(1);
                    } else {
                        seizure = "-" + seizure;
                    }
                }
                ptResult.setText(seizure);
            }
        });

        buttonPercent = findViewById(R.id.percent_b);
        buttonPercent.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                if (null == seizure) seizure = "0";
                if (op == null) {
                    t1 = Double.parseDouble(seizure) / 100;
                    seizure = String.valueOf(t1);
                    memory = seizure;
                    t1 = 0;
                } else {
// s'il y a un opérateur plus ou moins il faut calculer memory plus ou moins memory x seizure / 100
                    t2 = 0;
                }
                conc = false;
                ptResult.setText(memory);

            }
        });

        buttonSlash = findViewById(R.id.slash_b);
        buttonSlash.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                setSign("/");
            }
        });

        buttonMinus = findViewById(R.id.minus_b);
        buttonMinus.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                setSign("-");
            }
        });

        buttonPlus = findViewById(R.id.plus_b);
        buttonPlus.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                setSign("+");
            }
        });

        buttonTime = findViewById(R.id.time_b);
        buttonTime.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                setSign("*");
            }
        });

        button7 = findViewById(R.id.seven_b);
        button7.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                concat("7");
            }
        });

        button8 = findViewById(R.id.eight_b);
        button8.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                concat("8");
            }
        });

        button9 = findViewById(R.id.nine_b);
        button9.setOnClickListener(new View.OnClickListener() {
            // afficher seizure
            public void onClick(View v) {
                concat("9");
            }
        });

        button4 = findViewById(R.id.four_b);
        button4.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                concat("4");
            }
        });

        button5 = findViewById(R.id.five_b);
        button5.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                concat("5");
            }
        });

        button6 = findViewById(R.id.six_b);
        button6.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                concat("6");
            }
        });

        button1 = findViewById(R.id.one_b);
        button1.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                concat("1");
            }
        });

        button2 = findViewById(R.id.two_b);
        button2.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                concat("2");
            }
        });

        button3 = findViewById(R.id.three_b);
        button3.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                concat("3");
            }
        });

        button0 = findViewById(R.id.zero_b);
        button0.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                concat("0");
            }
        });

        buttonDot = findViewById(R.id.dot_b);
        buttonDot.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (true == conc) {
                    if (seizure == null) seizure = "0.";
                    if (!seizure.contains(".")) {
                        seizure = seizure + ".";
                    }
                } else {
                    seizure = "0.";
                    conc = true;
                }
                ptResult.setText(seizure);
            }
        });

        buttonEqual = findViewById(R.id.equal_b);
        buttonEqual.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (null != op) {
                compute();
                op = null;
                } else {
                    if (null != seizure) {
                        memory = seizure;
                    }
                    ptResult.setText(memory);
                }
                conc = false;
            }
        });

    }

    private void concat(String nb) {
        if(true == conc) {
            if (seizure != null) {
                seizure = seizure + nb;
            } else {
                seizure = nb;
            }
        } else {
                seizure = nb;
                conc = true;
        }
        ptResult.setText(seizure);
    }

    private void setSign(String sign) {
        if (null == seizure) {
            op = sign;
            conc = false;
        } else {
            if (null == op) {
                memory = seizure;
                op = sign;
                conc = false;
            } else {
                compute();
                op = sign;
            }
        }
        ptResult.setText(memory);
    }

    private void compute(){
        t1 = Double.parseDouble(memory);
        if (null != seizure) t2 = Double.parseDouble(seizure);

        if (true == conc) {
            switch (op) {
                case "null":
                    t2 = 0;
                    seizure = null;
                    conc = false;
                    ptResult.setText(memory);
                    break;
                case "+":
                    res = t1 + t2;
                    memory = String.valueOf(res);
                    //removeZeros();
                    ptResult.setText(memory);
                    seizure = null;
                    res = 0;
                    t1 = 0;
                    t2 = 0;
                    conc = false;
                    break;
                case "-":
                    res = t1 - t2;
                    memory = String.valueOf(res);
                    //removeZeros();
                    ptResult.setText(memory);
                    seizure = null;
                    res = 0;
                    t1 = 0;
                    t2 = 0;
                    conc = false;
                    //va falloir factoriser ces 4 à 7 lignes au-dessus !
                    break;
                case "*":
                    res = t1 * t2;
                    result = BigDecimal.valueOf(res).round(new MathContext(20, RoundingMode.HALF_EVEN));
                    res = result.doubleValue();
                    memory = String.valueOf(res);
                    //removeZeros();
                    ptResult.setText(memory);
                    seizure = null;
                    res = 0;
                    t1 = 0;
                    t2 = 0;
                    conc = false;
                    break;
                case "/":
                    if (0 != t2) {
                        term1 = BigDecimal.valueOf(t1);
                        term2 = BigDecimal.valueOf(t2);
                        result = term1.divide(term2, new MathContext(20, RoundingMode.HALF_EVEN));
                        res = result.doubleValue();
                        memory = String.valueOf(res);
                        //removeZeros();
                        ptResult.setText(memory);
                        seizure = null;
                        res = 0;
                        t1 = 0;
                        t2 = 0;
                        conc = false;
                    } else {
                        memory = getString(R.string.byZero);
                        ptResult.setText(memory);
                        memory = "0";
                        seizure = null;
                        t1 = 0;
                        t2 = 0;
                        op = null;
                        conc = false;
                    }
                    break;
            }
        }
    }

//    private void removeZeros() {
//        if (memory.contains(".")){
//            while ((memory.charAt(memory.length()-2)) == '0'){
//                if ((memory.charAt(memory.length()-1)) == '0'){
//                    memory = memory.substring(0, memory.length()-2);
//                }
//            }
//        }
//    }

    public void launchSecondActivity(View view) {
        Intent intent = new Intent(this, InfoActivity.class);
        startActivity(intent);
    }

}